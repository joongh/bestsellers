BestSellers
===================
#### Summary
The project to practice "React Native" The original code is from "Learning React Native 2/E by Bonnie Eisenman, ISBN 9781491989142".

This project is to show how to use FlatList component and SectionList component.

#### environment
<pre><code>
$ react-native info
Scanning folders for symlinks in /Users/joong/work/study/react_native/BestSellers/node_modules (15ms)
(node:58466) ExperimentalWarning: The fs.promises API is experimental

  React Native Environment Info:
    System:
      OS: macOS High Sierra 10.13.6
      CPU: x64 Intel(R) Core(TM) i5-5287U CPU @ 2.90GHz
      Memory: 42.65 MB / 8.00 GB
      Shell: 3.2.57 - /bin/bash
    Binaries:
      Node: 10.1.0 - ~/.nvm/versions/node/v10.1.0/bin/node
      Yarn: 1.9.4 - /usr/local/bin/yarn
      npm: 5.6.0 - ~/.nvm/versions/node/v10.1.0/bin/npm
      Watchman: 4.9.0 - /usr/local/bin/watchman
    SDKs:
      iOS SDK:
        Platforms: iOS 11.4, macOS 10.13, tvOS 11.4, watchOS 4.3
      Android SDK:
        Build Tools: 26.0.0, 26.0.3, 27.0.3
        API Levels: 24, 26, 27
    IDEs:
      Android Studio: 3.1 AI-173.4907809
      Xcode: 9.4.1/9F2000 - /usr/bin/xcodebuild
    npmPackages:
      react: 16.4.1 => 16.4.1 
      react-native: 0.56.0 => 0.56.0 
    npmGlobalPackages:
      create-react-native-app: 1.0.0
      react-native-cli: 2.0.1
</code></pre>